# MLTask2

## Objective

This project aims to implement 2 different Machine Learning Algorithms from the ground up:

* Naive Bayes Algorithm

* Algorithm to be chosen from ones learned at class

Afterwards, every algorithm will be tested on 4 separate datasets: 2 regression and 2 classification problems.

They will be compared with already implemented algorithms available in python libraries.

## Naive Bayes 

### Questions

#### What is the naive Bayes Algorithm?

Naive Bayes is a simple technique used in the contruction of classifiers, resulting not in one but various algorithms. Its main philosopy is that every value of a particular feature is idependent of every other.
Therefore, it consideres each value independently in order to make the correct prediction of such class.

#### Typical uses and contraints of the Naive Bayes Algorithm?

Naive Bayes are highly scalable and its simplicity and ease of use makes it fairly useful and robust for class prediction. Even though such is the case, its accuracy has remained sufficiently high with time, however, there are already present algorithms which have surpassed such results, though their implementation is not this simple.

### The Algorithm

Every classifier has a target and its features. The objective is to use such features to predict its target result.
The algorithm flow is:

1. Identification of all classes
2. Calculation of Probability Tables
    1. Identification of Data Type (Binominal, Multinominal, Numerical)
    2. Probability Models Estabilishment for each feature
    3. Probability Table Calculation
3. Prediction

## Regression Algorithm of Choice

### Questions

#### What is the Naive Bayes Algorithm?

#### Typical uses and contraints of the Naive Bayes Algorithm?

### The Algorithm