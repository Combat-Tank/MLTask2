import pandas as pd
import math
import numpy as np

def normpdf(x, mean, sd):
    var = float(sd)**2
    denom = (2*math.pi*var)**.5
    num = math.exp(-(float(x)-float(mean))**2/(2*var))
    return num/denom

def calc_prob(value, mean, sd):
    if sd == 0.0:
        if value == mean:
            return 1.0
        else:
            return 0.0
    else:
        return normpdf(value, mean, sd)

class NaiveBayes(object):
    def __init__(self):
        self.table = []
        self.possible_targets = []

    def fit(self, x_train, y_train):

        y_train = pd.Series(y_train, name='target')
        data = pd.DataFrame(x_train, y_train)
        self.table.clear()
        self.possible_targets = np.unique(y_train)

        for column in x_train:
            if (data[column].dtypes == int) or (data[column].dtypes == float):
                #Numerical data
                #Form vector of data with classes attached
                class_data = pd.concat([y_train,x_train[column]], axis=1)
                #Separate data into each class
                grouped_df = class_data[column].groupby(y_train)
                #Calculate Mean, STDev for each class
                mean = grouped_df.mean()
                stdv = grouped_df.std()
                #Append to table
                self.table.append(dict(zip(['Feature','Type','Table'],[column,'N',dict(zip(["mean","stdv"],[mean,stdv]))])))
            elif (data[column].dtypes == str) or (data[column].dtypes == object):
                #Nominal data
                #Form vector of data with classes attached
                class_data = pd.concat([y_train,x_train[column]], axis=1)
                #Count number of elements for each class and nominal category
                grouped_df = class_data.groupby(by=[column,'target']).size()
                #Calculate Probability Table
                target_prob = class_data.groupby('target').size().div(len(class_data))
                cond_prob = grouped_df.div(len(class_data)).div(target_prob, axis=0, level='target')
                #Append to table
                self.table.append(dict(zip(['Feature','Type','Table'],[column,'C',cond_prob])))
            else:
                print("ERROR")

    def predict(self, x_test):
        y_test = []
        for index,row in x_test.iterrows():
            prob = np.full(len(self.possible_targets),1.0)
            for feat in self.table:
                value = row[feat['Feature']]
                if feat['Type'] == 'N':
                    aux = feat['Table']
                    mean = feat['Table']['mean']
                    stdv = feat['Table']['stdv']
                    #calculate through normal distribution for each possible outcome
                    for i in range(len(self.possible_targets)):
                        prob[i] = prob[i] * calc_prob(value, mean[i], stdv[i])
                elif feat['Type'] == 'C':
                    #calculate through table
                    for i in range(len(self.possible_targets)):
                        if(feat['Table'].index.isin([(str(value),str(self.possible_targets[i]))]).any()):
                            prob[i] = prob[i] * feat['Table'][str(value)][str(self.possible_targets[i])]
                        else:
                            prob[i] = 0
                            continue
                else:
                    print('ERROR in getProb')
            max_index = np.argmax(prob)
            y_test.append(self.possible_targets[max_index])
        
        return np.asarray(y_test)