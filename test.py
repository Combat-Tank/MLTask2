import pandas as pd
import math
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
import sklearn.metrics as met
from NaiveBayes import *

df = pd.read_csv('Postures.csv', dtype={'Class':str,'User':str}, na_values='?')
df = df.iloc[1: , :]
df.fillna(0.0,inplace=True)
df[[col for col in df if df[col].dtype.kind != 'O']] += 100

train_set, test_set = train_test_split(df)

y_train = train_set['Class']

x_train = train_set.drop('Class',axis=1)

y_target = test_set['Class']

x_test = test_set.drop('Class',axis=1)

NB = NaiveBayes()

NB.fit(x_train=x_train,y_train=y_train)

y_test = NB.predict(x_test=x_test)

gnb = GaussianNB()

gnb_pred = gnb.fit(x_train,y_train).predict(x_test)

cm = met.confusion_matrix(y_target, y_test)
print("accuracy = " + str(met.accuracy_score(y_target, y_test)))
print("accuracyGNB = " + str(met.accuracy_score(y_target, gnb_pred)))